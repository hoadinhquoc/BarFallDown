﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour {

    [System.Serializable]
    public struct Stage
    {
        public int totalNode;
        public float speed;
    }

    //Cheat UI
    public Button ButtonRestart;
    public Text ScoreText;
    public Text HighScoreText;
    //
    [Header("Display Settings")]
    public GameObject theBarPrefab;
    public GameObject thePlanePrefab;

    

    public List<Color> ColorList;

    public Transform PlanePool;
    public Transform BarPool;

    private TheBar m_bar;
    private PlaneWithGap m_currentPlaneWithGap;

    [Header("Logic Settings")]

    public int defaultSize = 30;
    public float offetAccuracy = 0.2f;
    public int perfectCombo = 2;
    public int bonusPoint = 10;
    public float startPosY = 60f;
    public int minSize = 5;

    [Space(10)]
    public List<Stage> StageList;
    


    int currentSize;
    float m_currentPosY;
    public static Game Instance;

    private bool m_isFreeze = false;
    int m_previousColorIndex = 0;

    int m_perfectCount = 0;

    int m_score = 0;
    int m_highScore = 0;
    float m_fallingSpeed = 0;
    int m_stageIndex;
    int m_nodeIndex;

    enum State
    {
        INIT,
        MENU,
        GAMEPLAY,
        RESULT
    }

    State m_state = State.INIT;

    private void Awake()
    {
        Instance = this;
        Init();

        m_highScore = PlayerPrefs.GetInt("HighScore", 0);
        
    }

    private void Start()
    {
        StartGame();
        HighScoreText.text = m_highScore.ToString();
    }

    private void Init()
    {
        m_bar = Instantiate(theBarPrefab).GetComponent<TheBar>();
    }

    void StartGame()
    {
        foreach (Transform item in PlanePool)
            GameObject.Destroy(item.gameObject);
        foreach (Transform item in BarPool)
            GameObject.Destroy(item.gameObject);

        currentSize = defaultSize;
        m_currentPosY = 0f;
        m_isFreeze = false;

        m_stageIndex = 0;
        m_nodeIndex = 0;

        ButtonRestart.gameObject.SetActive(false);

        StartNewDrop(defaultSize);

        SetState(State.GAMEPLAY);

        if(m_score > m_highScore)
        {
            m_highScore = m_score;
            HighScoreText.text = m_highScore.ToString();
            PlayerPrefs.SetInt("HighScore", m_highScore);
            PlayerPrefs.Save();
        }

        m_score = 0;
        m_perfectCount = 0;
    }

    // Update is called once per frame
    void Update() {

        InputMgr.Instance.Update();

        switch (m_state)
        {
            case State.GAMEPLAY:

                if(!m_isFreeze)
                    m_bar.UpdateLogic();

                if (InputMgr.Instance.GetTouchState() == InputMgr.TouchState.DOWN)
                {
                    CheckResult();
                }

                if ((m_bar.transform.position.y - m_currentPlaneWithGap.GetCurrentPos()) * 10f < -currentSize - 0.5f)
                    GameOver();
                break;

            case State.RESULT:
                if (InputMgr.Instance.GetTouchState() == InputMgr.TouchState.DOWN)
                {
                    StartGame();
                }
                break;
        }
    }

    void CheckResult()
    {
        Vector3 barPos = m_bar.transform.position;
        float barPosY = barPos.y;
        float planePos = m_currentPlaneWithGap.GetCurrentPos();
        if (Mathf.Abs(barPosY - planePos) < offetAccuracy)
            barPos.y = planePos;
        else
            barPos.y = Mathf.Round(barPosY / 0.1f) * 0.1f;
        m_bar.transform.position = barPos;

        float deltaY = (barPos.y - m_currentPlaneWithGap.GetCurrentPos()) * 10f;

        int total = Mathf.Abs(Mathf.RoundToInt(deltaY));
        
        Debug.LogWarning("DeltaY " + deltaY.ToString() + " total " + total.ToString());

        if(deltaY > currentSize || deltaY < -currentSize || currentSize - total < minSize)
        {
            m_perfectCount = 0;
            //GameOver
            GameOver();
            return;
        }
        else if( deltaY > 0 && deltaY <= currentSize && total !=0)
        {
            Debug.Log("Bar over plane");
            m_bar.Cut(TheBar.CutInfo.TOP, total);
            //m_currentPlaneWithGap.Cut(PlaneWithGap.CutInfo.TOP, total);
            m_isFreeze = true;
            m_perfectCount = 0;
            m_currentPosY += ((float)currentSize) / 10f;
            currentSize -= total;
        }
        else if(total ==0 ||deltaY == 0 || float.IsPositiveInfinity(deltaY) || float.IsNegativeInfinity(deltaY))
        {
            //Perfect
            Debug.Log("Perfect");
            m_perfectCount++;
            
            m_currentPlaneWithGap.PlayPerfect();
            m_currentPosY += ((float)currentSize) / 10f;

            m_score += (currentSize * 2);

            if (m_perfectCount >= perfectCombo)
                currentSize += bonusPoint;

            StartNewDrop(currentSize, true);

        }
        else if (deltaY < 0 && deltaY >= -currentSize)
        {
            Debug.Log("Bar inside plane");
            //m_bar.Cut(TheBar.CutInfo.TOP, total);
            m_currentPlaneWithGap.Cut(PlaneWithGap.CutInfo.TOP, total);
            m_isFreeze = true;
            m_perfectCount = 0;
            currentSize -= total;
            m_currentPosY += ((float)currentSize) / 10f;
        }

        if(total != 0)
            m_score += currentSize;

        ScoreText.text = m_score.ToString();
    }

    public void NotifyFinishCut()
    {
        StartNewDrop(currentSize);
    }

    void StartNewDrop(int size, bool isPerfect = false)
    {
        m_fallingSpeed = StageList[m_stageIndex].speed;
        m_nodeIndex++;
        if (m_nodeIndex == StageList[m_stageIndex].totalNode)
        {
            m_nodeIndex = 0;
            m_stageIndex = (m_stageIndex + 1)  % StageList.Count;
        }

        m_isFreeze = false;

        float newScaleSize = ((float)size / (float)defaultSize);
        Vector3 newScale = new Vector3(newScaleSize, 1, newScaleSize);

        GameCamera.Instance.SetNextPos(m_currentPosY + size*0.1f - 3f);

        SpawnNewBar(size);
        SpawnNewPlane(size, isPerfect);
    }

    void SpawnNewBar(int size)
    {
        if (m_bar != null) Destroy(m_bar.gameObject);
        m_bar = Instantiate(theBarPrefab, BarPool).GetComponent<TheBar>();
        m_bar.SetVelocity(m_fallingSpeed);
        m_bar.SetStartPosY(startPosY + m_currentPosY * 10f + currentSize);
        m_bar.StartNewDrop(size);
    }

    void SpawnNewPlane(int size, bool isPerfect = false)
    {
        if(m_currentPlaneWithGap != null && !isPerfect)
            m_currentPlaneWithGap.EnableReplacementCube();
        m_currentPlaneWithGap = Instantiate(thePlanePrefab, PlanePool).GetComponent<PlaneWithGap>();

        int randomIndex = 0;

        if (isPerfect)
            randomIndex = m_previousColorIndex;
        else
        {
            //randomIndex = Random.Range(0, ColorList.Count);
            //while (m_previousColorIndex == randomIndex)
            //    randomIndex = Random.Range(0, ColorList.Count);
            randomIndex = (m_previousColorIndex+1)%ColorList.Count;
            m_previousColorIndex = randomIndex;
        }
        
        m_currentPlaneWithGap.Init(size, ColorList[randomIndex], isPerfect);
        m_bar.SetColor(ColorList[randomIndex]);
        
        m_currentPlaneWithGap.transform.position = new Vector3(0f, m_currentPosY, 0f);
    }

    void SetState(State state)
    {
        m_state = state;
    }

    public void OnRestartButtonPressed()
    {
        StartGame();
    }

    void GameOver()
    {
        Debug.Log("Gameover");
        SetState(State.RESULT);
        m_isFreeze = true;
        ButtonRestart.gameObject.SetActive(true);
    }

}
