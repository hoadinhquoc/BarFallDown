﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InputMgr
{

    public static InputMgr m_instance = null;
    private Vector2 m_touchScreenPosition;

    public enum TouchState
    {
		NONE,
        DOWN,
        DRAG,
        RELEASE
    }

	public enum SwipeAction 
	{
		NONE, 
		LEFT, 
		RIGHT, 
		UP, 
		DOWN, 
	}

	private SwipeAction swipeAction;

    private TouchState m_touchState = TouchState.RELEASE;

	private float fingerStartTime  = 0.0f;
	private Vector2 fingerStartPos = Vector2.zero;

	private bool isSwipe = false;
	private float minSwipeDist  = 50.0f;
	private float maxSwipeTime = 0.5f;

    public static InputMgr Instance
    {
        get
        {
            if (m_instance == null)
                m_instance = new InputMgr();

            return m_instance;
        }
    }

    InputMgr()
    {
        Input.simulateMouseWithTouches = true;
    }

	public SwipeAction GetSwipeAction()
	{
		return swipeAction;
	}

	public void ResetSwipe()
	{
		swipeAction = SwipeAction.NONE;
		swipeAction = SwipeAction.NONE;
		m_touchState = TouchState.NONE;
	}

    // Update is called once per frame
    public void Update()
    {
		m_touchState = TouchState.NONE;
#if UNITY_EDITOR || UNITY_WEBGL
        if (Input.GetMouseButtonDown(0))//Left click
        {
            m_touchState = TouchState.DOWN;
            m_touchScreenPosition = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            m_touchState = TouchState.RELEASE;
        }
        else if (Input.GetMouseButton(0))
        {
            m_touchState = TouchState.DRAG;
            m_touchScreenPosition = Input.mousePosition;

        }

		switch (m_touchState)
		{
		case TouchState.DOWN :
			/* this is a new touch */
			isSwipe = true;
			fingerStartTime = Time.time;
			fingerStartPos = Input.mousePosition;
			swipeAction = SwipeAction.NONE;
			break;

		case TouchState.RELEASE :

			float gestureTime = Time.time - fingerStartTime;
			float gestureDist = (m_touchScreenPosition - fingerStartPos).magnitude;

			if (isSwipe && gestureTime < maxSwipeTime && gestureDist > minSwipeDist){
				Vector2 direction = m_touchScreenPosition - fingerStartPos;
				Vector2 swipeType = Vector2.zero;

				if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y)){
					// the swipe is horizontal:
					swipeType = Vector2.right * Mathf.Sign(direction.x);
				}else{
					// the swipe is vertical:
					swipeType = Vector2.up * Mathf.Sign(direction.y);
				}

				if(swipeType.x != 0.0f){
					if(swipeType.x > 0.0f){
						swipeAction = SwipeAction.RIGHT;
						// MOVE RIGHT
					}else{
						swipeAction = SwipeAction.LEFT;
						// MOVE LEFT
					}
				}

				if(swipeType.y != 0.0f ){
					if(swipeType.y > 0.0f){
						swipeAction = SwipeAction.UP;
						// MOVE UP
					}else{
						// MOVE DOWN
					}
				}

			}

			break;
		}

#else
		///*
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            switch(touch.phase)
            {
                case TouchPhase.Began:
                    m_touchState = TouchState.DOWN;
                    m_touchScreenPosition = touch.position;
                    break;

                case TouchPhase.Moved:
                case TouchPhase.Stationary:
                    m_touchState = TouchState.DRAG;
                    m_touchScreenPosition = touch.position;
                    break;

                case TouchPhase.Ended:
                    m_touchState = TouchState.RELEASE;
                    break;
            }
        }
		//*/
		if (Input.touchCount > 0){

			foreach (Touch touch in Input.touches)
			{
				switch (touch.phase)
				{
				case TouchPhase.Began :
					/* this is a new touch */
					isSwipe = true;
					fingerStartTime = Time.time;
					fingerStartPos = touch.position;
					break;

				case TouchPhase.Canceled :
					/* The touch is being canceled */
					isSwipe = false;
					break;

				case TouchPhase.Ended :

					float gestureTime = Time.time - fingerStartTime;
					float gestureDist = (touch.position - fingerStartPos).magnitude;

					if (isSwipe && gestureTime < maxSwipeTime && gestureDist > minSwipeDist){
						Vector2 direction = touch.position - fingerStartPos;
						Vector2 swipeType = Vector2.zero;

						if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y)){
							// the swipe is horizontal:
							swipeType = Vector2.right * Mathf.Sign(direction.x);
						}else{
							// the swipe is vertical:
							swipeType = Vector2.up * Mathf.Sign(direction.y);
						}

						if(swipeType.x != 0.0f){
							if(swipeType.x > 0.0f){
								swipeAction = SwipeAction.RIGHT;
								// MOVE RIGHT
							}else{
								swipeAction = SwipeAction.LEFT;
								// MOVE LEFT
							}
						}

						if(swipeType.y != 0.0f ){
							if(swipeType.y > 0.0f){
								swipeAction = SwipeAction.UP;
								// MOVE UP
							}else{
								// MOVE DOWN
							}
						}

					}

					break;
				}
			}
		}

#endif
    }

    public TouchState GetTouchState()
    {
        return m_touchState;
    }

    public Vector2 GetTouchScreenPosition()
    {
        return m_touchScreenPosition;
    }
}
