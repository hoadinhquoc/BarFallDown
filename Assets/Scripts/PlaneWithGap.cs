﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneWithGap : MonoBehaviour {

    public GameObject oneUnitPrefab;
    public Transform replacementCube;
    int m_targetSize;
    List<GameObject> unitList;

    Color m_color = Color.red;

    //Cheat for Y
    readonly float hackY = 0.1f;

    public enum CutInfo
    {
        TOP,
        BOT,
    }

    Transform displayTransform; 

    void Awake()
    {
        displayTransform = transform.Find("Display").transform;
    }

    public void Init(int size, Color color, bool isPerfect = false)
    {
        m_color = color;
        m_targetSize = size;
        SetupDisplay(isPerfect);
    }

    void SetupDisplay(bool isPerfect = false)
    {
        unitList = new List<GameObject>();
        for (int i = 0; i < m_targetSize ; i++)
        {
            GameObject item = GameObject.Instantiate(oneUnitPrefab, displayTransform);
            
            item.transform.localPosition += new Vector3(0f, hackY * i, 0f);
            unitList.Add(item);

            //cheat color
            foreach(MeshRenderer renderer in item.GetComponentsInChildren<MeshRenderer>())
                renderer.material.color = m_color;
            //    renderer.material.SetColor("_TintColor", m_color);// = m_color;

            item.SetActive(false);
        }

        replacementCube.gameObject.SetActive(false);
        replacementCube.GetComponent<MeshRenderer>().material.color = m_color;
        Vector3 scale = replacementCube.localScale;
        scale.y = m_targetSize * 0.1f;
        replacementCube.localScale = scale;
        replacementCube.localPosition = new Vector3(0f, scale.y / 2f, 0f);

        if (isPerfect)
            AppearPerfect();
        else
            StartCoroutine(AppearCoroutine());
    }

    void AppearPerfect()
    {
        for (int i = 0; i < unitList.Count; i++)
        {
            unitList[i].SetActive(true);
            unitList[i].GetComponent<Animator>().Play("PerfectDancing");
        }
    }

    IEnumerator AppearCoroutine()
    {
        for(int i = unitList.Count - 1; i >= 0 ; i--)
        {
            unitList[i].SetActive(true);
            yield return new WaitForSeconds(0.3f/ unitList.Count);
        }
        yield return null;
    }

    void UpdateDisplay()
    {

    }
    
	// Update is called once per frame
	void Update () {


    }

    public float GetCurrentPos()
    {
        return transform.position.y;
    }

    public void SetPosY(float posY)
    {
        Vector3 pos = transform.position;
        pos.y = posY;

        transform.position = pos;
    }

    public void StartNewDrop(float newSize)
    {

    }

    public void Cut(CutInfo info, int total)
    {
        int startCutIndex = 0;
        int endCutIndex = 0;
        if (info == CutInfo.BOT)
        {
            startCutIndex = 0;
            endCutIndex = total;
        }
        else if (info == CutInfo.TOP)
        {
            startCutIndex = m_targetSize - total - 1;
            endCutIndex = m_targetSize - 1;

            //Calculate replacement Cube
            

            Vector3 scale = replacementCube.localScale;
            scale.y = (m_targetSize - total) * 0.1f;
            replacementCube.localScale = scale;
            replacementCube.localPosition = new Vector3(0f, scale.y/2f, 0f);
        }
        Debug.Log("Cut " + startCutIndex.ToString() + " " + endCutIndex.ToString());
        StartCoroutine(CutCoroutine(startCutIndex,endCutIndex));

    }

    IEnumerator CutCoroutine(int beginIndex,int endIndex)
    {
        for (int i = endIndex; i > beginIndex; i--)
        {
            unitList[i].GetComponent<Animator>().enabled = true;
            unitList[i].GetComponent<Animator>().Play("Disappear");
        }
        yield return new WaitForSeconds(0.7f);

        

        Game.Instance.NotifyFinishCut();
    }

    public void EnableReplacementCube()
    {
        unitList.ForEach(x => Destroy(x.gameObject));
        unitList.Clear();

        replacementCube.gameObject.SetActive(true);
    }

    public void PlayPerfect()
    {
        displayTransform.GetComponent<Animator>().enabled = true;
        displayTransform.GetComponent<Animator>().Play("Perfect");
    }

    public void Reset()
    {

    }
}
