﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheBar : MonoBehaviour {

    
    public GameObject oneUnitPrefab;
    int m_targetSize;

    //Cheat for Y
    readonly float hackY = 0.1f;

    Transform displayTransform;

    float m_velocity;
    float m_startPosY;

    List<GameObject> unitList;

    public enum CutInfo
    {
        TOP,
        BOT,
    }

    void Awake()
    {
        displayTransform = transform.Find("Display").transform;
    }

    public void Init(int size)
    {
        
    }

    void SetupDisplay(int size)
    {
        unitList = new List<GameObject>();
        foreach (Transform item in displayTransform)
            GameObject.Destroy(item.gameObject);
        for (int i = 0; i < size; i++)
        {
            GameObject item = GameObject.Instantiate(oneUnitPrefab, displayTransform);
            item.transform.localPosition += new Vector3(0f, hackY * i, 0f);
            unitList.Add(item);
        }
    }

    public void SetColor(Color color)
    {
        foreach (MeshRenderer renderer in displayTransform.GetComponentsInChildren<MeshRenderer>())
            renderer.material.color = color;
    }

    private void Update()
    {
        
    }

    public void UpdateLogic()
    {
        transform.position += (m_velocity * Vector3.down * Time.deltaTime);
    }

    // Update is called once per frame
    public void UpdateDisplay () {

        

	}


    public void StartNewDrop(int size, float previousPos = 0f)
    {
        m_targetSize = size;
        SetupDisplay(size);

        Vector3 startPos = transform.position;

        startPos.y = previousPos + size* hackY + m_startPosY;

        transform.position = startPos;
    }

    public float GetCurrentPos()
    {
        return transform.position.y;
    }

    public void SetVelocity(float velocity)
    {
        m_velocity = velocity/10f;
    }

    public void SetSize(int size)
    {
        m_targetSize = size;
        //SetupDisplay(size);
    }

    public void SetStartPosY(float posY)
    {
        m_startPosY = posY/10f;
    }

    public void Cut(CutInfo info, int total)
    {
        int startCutIndex = 0;
        int endCutIndex = 0;
        if(info == CutInfo.BOT)
        {
            startCutIndex = 0;
            endCutIndex = total;
        }
        else if(info == CutInfo.TOP)
        {
            Debug.Log("startCutIndex " + startCutIndex.ToString() + " " + total);
            startCutIndex = m_targetSize - total - 1;
            endCutIndex = m_targetSize - 1;
        }
        Debug.Log("Cut " + startCutIndex.ToString() + " " + endCutIndex.ToString());
        StartCoroutine(CutCoroutine(startCutIndex, endCutIndex));

    }

    IEnumerator CutCoroutine(int beginIndex, int endIndex)
    {
        for (int i = endIndex; i >beginIndex; i--)
        {
            unitList[i].GetComponent<Animator>().enabled = true;
            unitList[i].GetComponent<Animator>().Play("Disappear");
            yield return new WaitForSeconds(0.7f / (endIndex - beginIndex));
        }
        //yield return null;
        Game.Instance.NotifyFinishCut();
    }

    public void Reset()
    {

    }
}
