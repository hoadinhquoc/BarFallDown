﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour {

    public static GameCamera Instance;

    Vector3 m_nextPos;
    Vector3 defaultPos;

    float defaultOrthoSize = 8f;
    float m_nextOrthoSize;

    void Awake()
    {
        Instance = this;
        m_nextPos = transform.position;
        defaultPos = transform.position;

        defaultOrthoSize = Camera.main.orthographicSize;
        m_nextOrthoSize = Camera.main.orthographicSize;
    }
        // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.Lerp(transform.position, m_nextPos, Time.deltaTime);

        Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize , m_nextOrthoSize , Time.deltaTime);

    }

    public void SetNextPos(float posY)
    {
        m_nextPos = defaultPos + new Vector3(0f, posY, 0f);
    }

    public void ZoomOut(float value)
    {
        m_nextOrthoSize += value;
    }

    public void ZoomIn(float value)
    {
        m_nextOrthoSize -= value;
    }

    public void SetNextOrthoSize(float size)
    {
        m_nextOrthoSize = size;
    }

    public void Reset()
    {
        m_nextPos = defaultPos;
        m_nextOrthoSize = defaultOrthoSize;
    }
}
